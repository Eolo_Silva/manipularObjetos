﻿namespace CRUDListaObj
{
    using System.Windows.Forms;
    using Modelos;
    using System.Collections.Generic;
    using System;
    using System.IO;

    public partial class Form1 : Form
    {
        private List<Aluno> ListaDeAlunos;
        private FormNovoAluno FormularioNovoAluno;
        private FormApagarAluno FormularioApagarAluno;

        private Form1 formprincipal;

        public Form1()
        {
            InitializeComponent();
            ListaDeAlunos = new List<Aluno>();

            if (!CarregarAlunos())
            {
                ListaDeAlunos.Add(new Aluno { IdAluno = 16343, PrimeiroNome = "Amadeu", Apelido = "Antunes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16328, PrimeiroNome = "António", Apelido = "Almeida" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 14990, PrimeiroNome = "Daniel", Apelido = "Veiga" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16342, PrimeiroNome = "João", Apelido = "Ribeiro" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16333, PrimeiroNome = "Luís", Apelido = "Gomes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 12494, PrimeiroNome = "Hugo", Apelido = "Pereira" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16334, PrimeiroNome = "Nuno", Apelido = "Serrano" });
            }

            ComboBoxListaAlunos.DataSource = ListaDeAlunos;
            foreach (var aluno in ListaDeAlunos)
            {
                ComboBoxNomes.Items.Add(aluno.PrimeiroNome);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonProcurar_Click(object sender, EventArgs e)
        {
            if (ComboBoxProcurar.SelectedIndex == -1)
            {
                MessageBox.Show("Tem que escolher um critério");
                return;
            }

            int id = 0;

            if (ComboBoxProcurar.SelectedIndex == 0)
            {
                if (!int.TryParse(TextBoxProcurar.Text, out id))
                {
                    MessageBox.Show("Tem que escrever um número");
                    return;
                }

                var AlunoAchado = ProcurarAluno(id);

                if (AlunoAchado != null)
                {
                    MessageBox.Show(AlunoAchado.NomeCompleto);
                    TextBoxIdAluno.Text = AlunoAchado.IdAluno.ToString();
                    TextBoxNomeAluno.Text = AlunoAchado.NomeCompleto;
                }
                else
                {
                    MessageBox.Show("Esse id de aluno não existe");
                }
            }
            else
            {
                var AlunoAchado = ProcurarAluno(TextBoxProcurar.Text);

                if (AlunoAchado != null)
                {
                    MessageBox.Show("Esse aluno existe");
                    TextBoxIdAluno.Text = AlunoAchado.IdAluno.ToString();
                    TextBoxNomeAluno.Text = AlunoAchado.NomeCompleto;
                }
                else
                {
                    MessageBox.Show("Esse aluno não existe");
                }
            }



        }

        private Aluno ProcurarAluno(string nome)
        {
            foreach (var aluno in ListaDeAlunos)
            {
                if (aluno.PrimeiroNome == nome)
                {
                    return aluno;
                }

            }
            return null;

        }

        private Aluno ProcurarAluno(int id)
        {
            foreach(var aluno in ListaDeAlunos)
            {
                if (aluno.IdAluno == id)
                {
                    return aluno;
                }
               
            }
            return null;
        }

        private void ButtonEditar_Click(object sender, EventArgs e)
        {
            TextBoxPrimeiroNome.Enabled = true;
            TextBoxApelidoAluno.Enabled = true;
            ButtonGravar.Enabled = true;
            ButtonCancelar.Enabled = true;

        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            TextBoxPrimeiroNome.Enabled = false;
            TextBoxApelidoAluno.Enabled = false;
            ButtonGravar.Enabled = false;
            ButtonCancelar.Enabled = false;
        }

        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(TextBoxIdAluno.Text))
            {
                MessageBox.Show("Insira id de aluno");
                return;
            }

            if(string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Insira o primeiro nome");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxApelidoAluno.Text))
            {
                MessageBox.Show("Insira o apelido");
                return;
            }

            var AlunoAchado = ProcurarAluno(Convert.ToInt32(TextBoxIdAluno.Text));

            if (AlunoAchado != null)
            {
                AlunoAchado.PrimeiroNome = TextBoxPrimeiroNome.Text;
                AlunoAchado.Apelido = TextBoxApelidoAluno.Text;
                MessageBox.Show("Aluno alterado com sucesso");

                ActualizaCombos();

                TextBoxPrimeiroNome.Text = string.Empty;
                TextBoxPrimeiroNome.Enabled = false;

                TextBoxApelidoAluno.Text = string.Empty;
                TextBoxApelidoAluno.Enabled = false;

                ButtonGravar.Enabled = false;
                ButtonCancelar.Enabled = false;
            }

            
        }

        private void ActualizaCombos()
        {
            ComboBoxListaAlunos.DataSource = null;
            ComboBoxListaAlunos.DataSource = ListaDeAlunos;

            ComboBoxNomes.Items.Clear();

            foreach (var aluno in ListaDeAlunos)
            {
                ComboBoxNomes.Items.Add(aluno.PrimeiroNome);
            }

        }

        private bool GravarAlunos()
        {
            string ficheiro = @"Alunos.txt";

            StreamWriter sw = new StreamWriter(ficheiro, false);

            try
            {
                if (!File.Exists(ficheiro))
                {
                    sw = File.CreateText(ficheiro);
                }

                foreach (var aluno in ListaDeAlunos)
                {
                    string linha = string.Format("{0};{1};{2}", aluno.IdAluno, aluno.PrimeiroNome, aluno.Apelido);
                    sw.WriteLine(linha);

                }

                sw.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }


            return true;
        }

        private bool CarregarAlunos()
        {
            string ficheiro = @"Alunos.txt";

            StreamReader sr;

            try
            {
                if(File.Exists(ficheiro))
                {
                    sr = File.OpenText(ficheiro);
                    string linha = "";

                    while((linha = sr.ReadLine()) != null)
                    {
                        string[] campos = new string[3];
                        campos = linha.Split(';');

                        var aluno = new Aluno
                        {
                            IdAluno = Convert.ToInt32(campos[0]),
                            PrimeiroNome = campos[1],
                            Apelido = campos[2]
                        };

                        ListaDeAlunos.Add(aluno);
                    }

                    sr.Close();
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;

            }


            return true;

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(GravarAlunos())
            {
                MessageBox.Show("Alunos Gravados Com Sucesso");
            }

        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormularioNovoAluno = new FormNovoAluno(ListaDeAlunos);
            FormularioNovoAluno.Show();
        }

        private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActualizaCombos();
            MessageBox.Show("Alunos Atualizados com sucesso.");
        }

        private void apagarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxIdAluno.Text))
            {
                MessageBox.Show("Insira Id a procurar.");
                return;
            }

            var alunoapagado = ProcurarAluno(Convert.ToInt32(TextBoxIdAluno.Text));
       
            FormularioApagarAluno = new FormApagarAluno(ListaDeAlunos, alunoapagado);

            FormularioApagarAluno.Show();
        }
    }
}
