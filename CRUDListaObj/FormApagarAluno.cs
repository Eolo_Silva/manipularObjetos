﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUDListaObj
{
    using Modelos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormApagarAluno : Form
    {
        private List<Aluno> ListaDeAlunos;

        private Aluno alunoAApagar;
        public FormApagarAluno(List<Aluno>listaAlunos, Aluno aluno)
        {
            InitializeComponent();

            

            this.ListaDeAlunos = listaAlunos;
            alunoAApagar = aluno;

            DataGridViewApagar.Rows.Add(alunoAApagar.IdAluno, alunoAApagar.PrimeiroNome, alunoAApagar.Apelido);
            DataGridViewApagar.AllowUserToAddRows = false;
        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O aluno não foi apagado");
            Close();
        }

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            int index = 0;
            foreach (var aluno in ListaDeAlunos)
            {
                if(alunoAApagar.IdAluno == aluno.IdAluno)
                {
                    ListaDeAlunos.RemoveAt(index);
                    break;
                }

                index++;
            }

            MessageBox.Show("Aluno Apagado com sucesso");
        
            Close();
        }
    }
}
