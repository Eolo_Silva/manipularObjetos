﻿namespace CRUDListaObj
{
    using CRUDListaObj.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormNovoAluno : Form
    {

        private List<Aluno> ListaDeAlunos = new List<Aluno>();

        public FormNovoAluno(List<Aluno>ListaDeAlunos)
        {
            InitializeComponent();
            this.ListaDeAlunos = ListaDeAlunos;
        }

        private void ButtonNovoAluno_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Insira o primeiro nome de aluno");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxApelido.Text))
            {
                MessageBox.Show("Insira o apelido");
                return;
            }


            var aluno = new Aluno
            {
                IdAluno = GerarIdAluno(),
                PrimeiroNome = TextBoxPrimeiroNome.Text,
                Apelido = TextBoxApelido.Text
            };

            ListaDeAlunos.Add(aluno);
            MessageBox.Show("Novo Aluno inserido com sucesso");
            Close();
        }

        private int GerarIdAluno()
        {
            return ListaDeAlunos[ListaDeAlunos.Count - 1].IdAluno + 10;
        }
    }
}
